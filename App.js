import React, {useEffect} from 'react';
import Navigation from './Navigation';
import {GlobalProvider} from './src/store/GlobalProvider';
import SplashScreen from 'react-native-splash-screen'

export default function App() {
  
  useEffect(() => {
    SplashScreen.hide();
  });

  return (
    <GlobalProvider>
      <Navigation/>  
    </GlobalProvider>       
  );
};
