import React, { useContext, useState, useEffect } from 'react';
import {Text, View, SafeAreaView, TouchableOpacity, StyleSheet, 
  Dimensions, TextInput, ScrollView, Modal, Alert} from 'react-native';
import {CustomeHeader} from '../../common/CustomeHeader';
import {GlobalContext} from '../../store/GlobalProvider';

import FontAwesome from 'react-native-vector-icons/FontAwesome';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import { Menu, Divider, Provider as PaperProvider } from 'react-native-paper';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { CheckBox } from 'react-native-elements'
import { useIsFocused } from '@react-navigation/native';

import {callNumber} from '../../common/CommonFunction';
import {DropDown} from '../../common/DropDown';
import {codh, niemchi} from '../../common/Constant';
import {ModalPicker_KhachHang} from '../../components/dungchung/ModalPicker_KhachHang';
import {ModalPicker_NhanVien} from '../../components/dungchung/ModalPicker_NhanVien';
import {Loading} from '../../common/Loading';

import {thayDongHoDinhKy, uploadImageToServer, getListVatTuSuaChua} from '../../api/Api_CongNhan';

const { width, height } = Dimensions.get('window');

export function TaoThongTinSuaChua_CongNhan_Screen ({navigation, route}) {

  var isFocused = useIsFocused();

  const global = useContext(GlobalContext);

  let url = global.url;
  let username = global.username;
  let password = global.password;
  let tennv = global.fullname;
  let listloaixl = global.listloaixl;
  let listloaidongho = global.listloaidongho;

  const type = 'suachua';

  const[madon, setMadon] = useState('');
  const[idkh, setIdkh] = useState('');
  const[danhbo, setDanhbo] = useState('');
  const[tenkh, setTenkh] = useState('');
  const[diachi, setDiachi] = useState('');
  const[sdt, setSdt] = useState('');
  const[thongtinkh, setThongtinkh] = useState('');
  const[noidung, setNoiDung] = useState('');
  const[loaixl, setLoaixl] = useState('');
  const[lydo, setLydo] = useState('');
  const[bienphap, setBienphap] = useState('');
  const[duongkinhdh, setDuongkinhdh] = useState('');
  const[loaidh, setLoaidh] = useState('');
  const[niem3chi, setNiem3chi] = useState('');
  const[serial, setSerial] = useState('');
  const[csc, setCsc] = useState('');
  const[csm, setCsm] = useState('');
  const[manvxl2, setManvxl2] = useState('');
  const[ghichu, setGhichu] = useState('');

  const[isloading, setIsloading] = useState();
  const [visible, setVisible] = useState(false);

  const setValueDropDown = (option) => {
    setLoaixl(option);
  } 

  const setValueDropDown_LoaiDh = (option) => {
    setLoaidh(option);
  } 

  const setValueDropDown_Niemchi = (option) => {
    setNiem3chi(option);
  } 

  const setValueDropDown_DuongKinhDh= (option) => {
    setDuongkinhdh(option);
  } 

  const openMenu = () => setVisible(true);

  const closeMenu = () => setVisible(false);
 
  const [isModal_KH_Visible, setIsModal_KH_Visible] = useState(false);

  const changeModal_KH_Visible = (bool) => {
    setIsModal_KH_Visible(bool);
    }

  const [isModal_NV_Visible, setIsModal_NV_Visible] = useState(false);

  const changeModal_NV_Visible = (bool) => {
    setIsModal_NV_Visible(bool);
  }
  const setData = (option) => {
    setIdkh(option.idkh);
    setDanhbo(option.danhbo);
    setTenkh(option.tenkh);
    setDiachi(option.diachi);
    setSdt(option.sdt);
  }  
  
  const setData_NhanVien = (option) => {
    setManvxl2(option.manv);
  }   

  const [listVatTuSuaChua, setListVatTuSuaChua] = useState([]);

  deleteItemById = id => {
    const filteredData = listVatTuSuaChua.filter(item => item.mavattu !== id);
    setListVatTuSuaChua(filteredData);
  }

  let options = {
      title: 'Choose an Image',
      quality: 1.0,
      maxHeight: 900, 
      maxWidth: 675, 
      includeBase64 : true,
      storageOptions: {
        skipBackup: true,
      },
    };

    chooseImage = () => {   
      launchImageLibrary(options, (response) => {
        if (response.didCancel) {
          console.log('User cancelled image picker');
        } else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        } else if (response.customButton) {
          console.log('User tapped custom button: ', response.customButton);
          alert(response.customButton);
        }else if (response.assets[0].fileSize > 5242880) {
          alert("Oops! the photos are too big. Max photo size is 4MB per photo. Please reduce the resolution or file size and retry");
      } else {
          setIsloading(false);
          let base64 =  response.assets[0].base64;
          _uploadImageToServer(base64);
        }
      });
  }

  captureImage = () => {       
    launchCamera(options, (response) => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
        alert(response.customButton);
      }else {
        try
        {
          if(response.assets != null)
          {
            setIsloading(false);
            let base64 =  response.assets[0].base64;
            _uploadImageToServer(base64);
          }
          else
          {
            console.log('error ios, android ok');
          }
        }
        catch(error)
        {
          console.log(`Error is : ${error}`);
        }        
      }
    });    
}

useEffect(()=> {
  if(isFocused){
    _getListVatTuSuaChua();
  }
  return () => {
    isFocused = false // add this
   }
}, [isFocused]);

const clearData = () => {
  setMadon('');
  setIdkh('');
  setDanhbo('');
  setTenkh('');
  setDiachi('');
  setSdt('');
  setThongtinkh('');
  setNoiDung('');
  setLoaixl('');
  setLydo('');
  setBienphap('');
  setDuongkinhdh();
  setLoaidh();
  setNiem3chi();
  setSerial('');
  setCsc('');
  setCsm('');
  setManvxl2('');
  setGhichu('');
  setListVatTuSuaChua([]);
} 

const _capNhatSuaChua = async () => { 
  thayDongHoDinhKy(url, username, password, madon, idkh, tenkh, 
    sdt, thongtinkh, noidung, loaixl, lydo, bienphap, duongkinhdh, loaidh,
    niem3chi, serial, csc, csm, manvxl2, ghichu) 
    .then(obj => {
      // console.log(url, username, password, madon, idkh, tenkh, 
      //   sdt, thongtinkh, noidung, loaixl, lydo, bienphap, duongkinhdh, loaidh,
      //   niem3chi, serial, csc, csm, manvxl2, ghichu);
        if(obj.ResultCode === true){   
          setIsloading(true); 
          if(madon == '')
          {
            setMadon(obj.Data);
            alert('Tạo mới thông tin sửa chữa thành công.');
          }
          else
          {
            clearData();  
            alert('Cập nhật dữ liệu thành công.');  
          }          
        }   
        else{
          setIsloading(true);  
          alert('Cập nhật dữ liệu KHÔNG thành công.');      
        }     
      })
      .catch(error => {
          setIsloading(true); 
          alert('lỗi : ' + error);
      });
}

const _uploadImageToServer = async (base64) => { 
  uploadImageToServer(url, username, password, 'SUACHUA_' + madon, 0, 0, base64) 
    .then(obj => {
        if(obj.ResultCode === true){   
          alert('Upload ảnh thành công');      
        }   
        else{
          alert('Upload ảnh KHÔNG thành công.');       
        }   
        setIsloading(true);      
      })
      .catch(error => {
          setIsloading(true);
          alert('lỗi : ' + error);
      });
}

const _getListVatTuSuaChua = async () => { 
  getListVatTuSuaChua(url, username, password, madon) 
    .then(obj => {
        if(obj.ResultCode === true){   
          setListVatTuSuaChua(obj.Data); 
        }       
      })
      .catch(error => {
          alert('lỗi : ' + error);
      });
}
    return (
        <SafeAreaView 
          style={{flex: 1}}
          >
          <CustomeHeader
            title = 'Tạo thông tin sửa chữa'
            isHome={false}
            navigation={navigation} 
          />         
          <View style = {{flex : 1}}>
            <View style = {{flex : 8}}>
              <ScrollView>
                    <View style = {styles.wrapper}/>                       
            
                    <View style = {styles.rowContainer}>                                                     
                          <View style = {styles.rows}>
                            <Text style = {styles.title}>Mã đơn</Text>
                            <View style = {styles.item}>
                              <TextInput 
                                style = {styles.textInput}  
                                editable={false}
                                value = {madon}
                              />  
                            </View>
                          </View>                              
                    </View>   

                    <View style = {styles.wrapper}/>  

                    <View style = {styles.rowContainer}>                                                    
                          <View style = {styles.rows}>
                            <Text style = {{flex : 3.2, 
                              fontSize : 15}}>
                                IDKH
                            </Text>
                            <View style = {{ flex : 7, 
                              backgroundColor : '#fffaf0', 
                              }}>
                              <TextInput 
                                style = {styles.textInput} 
                                editable={false}
                                value = {idkh}
                              /> 
                            </View>
                            <View style = {[styles.iconPhone, {marginLeft : 10, flexDirection : 'row'}]}>
                              <View style = {{flex : 2}}>
                                <TouchableOpacity 
                                  onPress = {() => {
                                    changeModal_KH_Visible(true);
                                  }}
                                >
                                  <MaterialCommunityIcons 
                                    name = 'account-search-outline'
                                    size = {40}
                                    />
                                </TouchableOpacity>  
                              </View>
                              <View style = {{flex : 3}}/>                          
                            </View>
                          </View>   
                          <Modal            
                              transparent = {true}
                              animationType = 'fade'
                              visible = {isModal_KH_Visible}
                              onRequestClose = {() => changeModal_KH_Visible(false)}
                          >
                              <ModalPicker_KhachHang 
                                changeModalVisible = {changeModal_KH_Visible}
                                setData = {setData}
                              />
                          </Modal>                              
                     </View>    

                    <View style = {styles.wrapper}/>  

                    <View style = {styles.rowContainer}>                                                    
                          <View style = {styles.rows}>
                            <Text style = {{flex : 3.2, 
                              fontSize : 15}}>
                                Danh bộ
                            </Text>
                            <View style = {{ flex : 7, 
                              backgroundColor : '#fffaf0', 
                              }}>
                              <TextInput 
                                style = {styles.textInput} 
                                editable={false}
                                value = {danhbo} 
                              /> 
                            </View>
                            <View style = {[styles.iconPhone, {marginLeft : 10}]}>                         
                            </View>
                          </View>                              
                    </View>    
                    <View style = {styles.wrapper}/>                      
            
                    <View style = {styles.rowContainer}>                                                    
                          <View style = {styles.rows}>
                            <Text style = {styles.title}>Tên KH</Text>
                            <View style = {styles.item}>
                              <TextInput 
                                style = {styles.textInput}
                                value = {tenkh}
                                onChangeText = {(text) => {
                                  setTenkh(text);
                                  }}
                              />
                            </View>
                          </View>                              
                    </View>   

                    <View style = {styles.wrapper}/>                       
            
                    <View style = {styles.rowContainer}>                                                     
                          <View style = {styles.rows}>
                            <Text style = {styles.title}>Địa chỉ</Text>
                            <View style = {styles.item}>
                              <TextInput 
                                multiline = {true}
                                style = {styles.textInput} 
                                value = {diachi}
                                onChangeText = {(text) => {
                                  setDiachi(text);
                                  }}
                              />
                            </View>
                          </View>                              
                    </View>  

                    <View style = {styles.wrapper}/>                       
            
                    <View style = {styles.rowContainer}>                                                    
                          <View style = {styles.rows}>
                            <Text style = {{flex : 3.2, 
                              fontSize : 15}}>
                                Sđt
                            </Text>
                            <View style = {{ flex : 7, 
                              backgroundColor : '#fffaf0', 
                              }}>
                              <TextInput 
                                style = {styles.textInput}
                                value = {sdt}
                                keyboardType="number-pad"
                                onChangeText = {(text) => {
                                  setSdt(text);
                                  }}
                              /> 
                            </View>
                            <View style = {[styles.iconPhone, {flexDirection : 'row', marginLeft : 10}]}>
                              <View style = {{flex : 2}}>
                                <TouchableOpacity 
                                  onPress = {() => {
                                    callNumber(sdt);
                                  }}
                                >
                                  <FontAwesome 
                                    name = 'phone-square'
                                    color = {'#000000'}
                                    size = {40}
                                    />
                                </TouchableOpacity>    
                              </View> 
                              <View style = {{flex : 3}}></View>                                                      
                            </View>
                          </View>                              
                    </View>    

                    <View style = {styles.wrapper}/> 

                    <View style = {styles.rowContainer}>                                                       
                        <View style = {styles.rows}>
                          <Text style = {styles.title}>Loại xử lý</Text>
                          <View style = {{flex : 7, 
                            marginRight : 5}}>                            
                            <DropDown 
                              list = {listloaixl} 
                              setValueDropDown = {setValueDropDown}
                            />        
                          </View>
                        </View>                              
                    </View>     

                    <View style = {styles.wrapper}/>   

                    <View style = {styles.rowContainer}>                                                       
                        <View style = {styles.rows}>
                          <Text style = {styles.title}>Cỡ ĐH</Text>
                          <View style = {{flex : 1.9, 
                            marginRight : 5}}>                            
                            <DropDown defaultValue = {'Chọn'} 
                              list = {codh} 
                              setValueDropDown = {setValueDropDown_DuongKinhDh}/>        
                          </View>
                          <View style = {{flex : 5, 
                            marginLeft: 5, flexDirection : 'row', alignItems : 'center', justifyContent : 'center'}}>
                            <Text style = {styles.title}>Loại</Text>
                            <View style = {{flex : 8, marginRight : 5}}>
                              <DropDown 
                                list = {listloaidongho} 
                                setValueDropDown = {setValueDropDown_LoaiDh}/>
                            </View>                         
                          </View>
                        </View>                              
                    </View>     

                    <View style = {styles.wrapper}/> 

                    <View style = {styles.rowContainer}>                                                       
                        <View style = {styles.rows}>
                          <Text style = {styles.title}>Niêm chì</Text>
                          <View style = {{flex : 3, 
                            marginRight : 5}}>                            
                            <DropDown 
                            defaultValue = '' 
                            list = {niemchi} 
                            setValueDropDown = {setValueDropDown_Niemchi}/>        
                          </View>
                          <View style = {{flex : 4, 
                            marginLeft: 5, flexDirection : 'row', alignItems : 'center', justifyContent : 'center'}}>
                            <Text style = {styles.title}>Số serial</Text>
                            <View style = {{flex : 5, marginRight : 5, marginLeft : 5}}>
                              <TextInput 
                                style = {styles.textInput} 
                                value = {serial}
                                onChangeText = {(text) => {
                                  setSerial(text);
                                  }}
                              />   
                            </View>
                           
                          </View>
                        </View>                              
                    </View>     
                  
                    <View style = {styles.wrapper}/> 

                    <View style = {styles.rowContainer}>                                                       
                        <View style = {styles.rows}>
                          <Text style = {styles.title}>Thông tin</Text>
                          <View style = {styles.item}>                            
                            <TextInput 
                              style = {styles.textInput} 
                              multiline = {true}
                              maxLength = {150}
                              placeholder = 'Thêm thông tin'
                              value = {thongtinkh}
                              onChangeText = {(text) => {
                                setThongtinkh(text);
                                }}
                            />                        
                          </View>
                        </View>                              
                    </View>                      
                    
                    <View style = {styles.wrapper}/>
                     
                    <View style = {styles.rowContainer}>                                                       
                        <View style = {styles.rows}>
                          <Text style = {styles.title}>Nội dung</Text>
                          <View style = {styles.item}>                            
                            <TextInput 
                              style = {styles.textInput} 
                              multiline = {true}
                              maxLength = {150}
                              placeholder = 'Thêm nội dung'
                              value = {noidung}
                              onChangeText = {(text) => {
                                setNoiDung(text);
                                }}
                            />                        
                          </View>
                        </View>                              
                    </View>                      
                    
                    <View style = {styles.wrapper}/>                

                    <View style = {styles.rowContainer}>                                                       
                        <View style = {styles.rows}>
                          <Text style = {styles.title}>Lý do xử lý</Text>
                          <View style = {styles.item}>                            
                            <TextInput 
                              style = {styles.textInput} 
                              multiline = {true}
                              maxLength = {150}
                              placeholder = 'Thêm lý do xử lý'
                              value = {lydo}
                              onChangeText = {(text) => {
                                setLydo(text);
                                }}
                            />                        
                          </View>
                        </View>                              
                    </View>  

                    <View style = {styles.wrapper}/>  

                    <View style = {styles.rowContainer}>                                                       
                        <View style = {styles.rows}>
                          <Text style = {styles.title}>Biện pháp xl</Text>
                          <View style = {styles.item}>                            
                            <TextInput 
                              style = {styles.textInput} 
                              multiline = {true}
                              maxLength = {150}
                              placeholder = 'Thêm biên pháp xử lý'
                              value = {bienphap}
                              onChangeText = {(text) => {
                                setBienphap(text);
                                }}
                            />                        
                          </View>
                        </View>                              
                    </View>                     
                    
                    <View style = {styles.wrapper}/>

                    <View style = {styles.rowContainer}>                                                       
                        <View style = {styles.rows}>
                          <Text style = {styles.title}>CS cũ</Text>
                          <View style = {{flex : 3, 
                            marginRight : 5}}>                            
                            <TextInput 
                                style = {styles.textInput} 
                                value = {csc}
                                keyboardType="number-pad"
                                onChangeText = {(text) => {
                                  setCsc(text);
                                  }}
                              />  
                          </View>
                          <View style = {{flex : 4, 
                            marginLeft: 5, flexDirection : 'row', 
                            alignItems : 'center', justifyContent : 'center'}}>
                            <Text style = {styles.title}>CS mới</Text>
                            <View style = {{flex : 4, marginRight : 5, marginLeft : 5}}>
                              <TextInput 
                                style = {styles.textInput} 
                                value = {csm}
                                keyboardType="number-pad"
                                onChangeText = {(text) => {
                                  setCsm(text);
                                  }}
                              />   
                            </View>
                           
                          </View>
                        </View>                              
                    </View>     

                    <View style = {styles.wrapper}/>

                    <View style = {styles.rowContainer}>                                                       
                        <View style = {styles.rows}>
                          <Text style = {styles.title}>NVXL</Text>
                          <View style = {{flex : 3, 
                            marginRight : 5}}>                            
                            <TextInput 
                                style = {styles.textInput} 
                                value = {username}
                                editable ={false}
                                onChangeText = {(text) => {
                                  }}
                              />  
                          </View>
                          <View style = {{flex : 4, 
                            marginLeft: 5, flexDirection : 'row', 
                            alignItems : 'center', justifyContent : 'center'}}>
                            <Text style = {styles.title}></Text>
                            <View style = {{flex : 4, marginRight : 5, marginLeft : 5}}>
                            </View>
                           
                          </View>
                        </View>                              
                    </View>     

                    <View style = {styles.wrapper}/>

                    <View style = {styles.rowContainer}>                                                       
                        <View style = {styles.rows}>
                          <Text style = {styles.title}>Tên NV</Text>
                          <View style = {{flex : 7, 
                            marginRight : 5}}>                            
                            <TextInput 
                                style = {styles.textInput} 
                                editable = {false}
                                value = {tennv}
                                onChangeText = {(text) => {
                                  }}
                              />  
                          </View>
                        </View>                              
                    </View>     

                    <View style = {styles.wrapper}/>

                    <View style = {styles.rowContainer}>                                                    
                          <View style = {styles.rows}>
                            <Text style = {{flex : 2.3, 
                              fontSize : 15}}>
                                NVXL 2
                            </Text>
                            <View style = {{ flex : 4, 
                              backgroundColor : '#fffaf0', 
                              }}>
                              <TextInput 
                                style = {styles.textInput} 
                                value = {manvxl2}
                                onChangeText = {(text) => {
                                  setManvxl2(text);
                                }}
                              /> 
                            </View>
                            <View style = {[styles.iconPhone, {marginLeft : 10, flexDirection : 'row'}]}>
                              <View style = {{flex : 1}}>
                                <TouchableOpacity 
                                  onPress = {() => {
                                    changeModal_NV_Visible(true);
                                  }}
                                >
                                  <MaterialCommunityIcons 
                                    name = 'account-search-outline'
                                    size = {40}
                                    />
                                </TouchableOpacity>      
                              </View>
                              <View style = {{flex : 3}}/>                      
                            </View>
                          </View>   
                          <Modal            
                              transparent = {true}
                              animationType = 'fade'
                              visible = {isModal_NV_Visible}
                              onRequestClose = {() => changeModal_NV_Visible(false)}
                          >
                              <ModalPicker_NhanVien 
                                changeModalVisible = {changeModal_NV_Visible}
                                setData_NhanVien = {setData_NhanVien}
                              />
                          </Modal>                              
                     </View>    

                    <View style = {styles.wrapper}/>

                    <View style = {styles.rowContainer}>                                                       
                        <View style = {styles.rows}>
                          <Text style = {styles.title}>Ghi chú</Text>
                          <View style = {styles.item}>                            
                            <TextInput 
                              style = {styles.textInput} 
                              multiline = {true}
                              maxLength = {150}
                              placeholder = 'Thêm ghi chú'
                              value = {ghichu}
                              onChangeText = {(text) => {
                                setGhichu(text);
                                }}
                            />                        
                          </View>
                        </View>                              
                    </View>                      
                    
                    <View style = {styles.wrapper}/>

                    <View style = {{
                      flex : 1, 
                      backgroundColor : 'white',
                      alignItems : 'center', 
                      justifyContent : 'center',
                      height : 30
                      }}>
                      <Text style = {styles.titleVT}>DANH SÁCH VẬT TƯ</Text> 
                    </View>  
                    <View style = {styles.wrapper}/> 
                    <View style = {{flex : 1, alignItems : 'center', justifyContent : 'center'}}>
                      <View style = {styles.viewContainerFlatlist}>
                            <Text style={styles.tenvattu}>Tên vật tư</Text>  
                            <Text style={styles.slcongty}>SL Cty</Text>
                            <Text style={styles.slkhachhang}>SL Kh</Text>
                            <Text style={styles.deleteBtn}></Text>
                        </View>
                    </View>
                    <View style = {styles.wrapper}/>     

                    <View style = {{backgroundColor : 'white'}}>
                      {/* thay dòng này để ko dùng flatlist để ko lỗi 
                      VirtualizedLists should never be nested inside plain ScrollViews */}
                      {listVatTuSuaChua.map((item, index) => (
                           <FlatListItemVatTuThuongDung item = {item} key = {index}/>
                      ))}
                    </View>                     
                </ScrollView>
                <PaperProvider>
                  <View style = {styles.anchor}>
                    <Menu
                      visible={visible}
                      onDismiss={closeMenu}
                      anchor={{x : width - 20, y : height / 1.7}}
                    >
                      <Menu.Item 
                        title="Camera" 
                        onPress={() => 
                        { 
                          closeMenu();
                          captureImage();
                        }
                        }  
                      />
                      <Divider />
                      <Menu.Item 
                        title="Image Library"
                        onPress={() => 
                        { 
                          closeMenu();
                          chooseImage();
                        }}  
                      />
                    </Menu>
                  </View>
                </PaperProvider>
            </View>
            <View style = {styles.wrapper}/>           
            <View style = {{flex : 1}}>                   
                      <View style = {{
                          flex: 1,
                          flexDirection : 'row', 
                          justifyContent: 'space-around'
                        }}>
                        <TouchableOpacity 
                          style = {styles.btnButton}
                          onPress = {() => 
                            {
                              if(loaixl == null || loaixl == '')
                              {
                                alert('Chọn loại xử lý.');
                                return;
                              }

                              setIsloading(false);
                              _capNhatSuaChua();
                            }
                          }>
                            <Text style = {styles.submitText}>Cập nhật</Text>
                        </TouchableOpacity>
                        <TouchableOpacity 
                          style = {styles.btnButton}
                          onPress = {() => 
                            {
                              if(madon == '')
                              {
                                alert('Không tìm thấy đơn sửa chữa, không thể nhập vật tư.');
                                return;
                              }

                              navigation.navigate('TimKiemVatTu', {type, madon});
                            }
                          }>
                            <Text style = {styles.submitText}>Kê vật tư</Text>
                        </TouchableOpacity>
                        <TouchableOpacity 
                          style = {styles.btnButton}
                          onPress = {() => 
                            {
                              if(madon == '')
                              {
                                alert('Không tìm thấy đơn sửa chữa, không thể chụp hình.');
                                return;
                              }

                              openMenu();
                            }
                          }>
                            <Text style = {styles.submitText}>Chụp hình</Text>
                        </TouchableOpacity>
                      </View>                            
                  </View>    
          </View>
          <>
            { 
                //để thể hiện đang loading 
                isloading === false
                ?
                  <Loading />
                :
                null
             }   
          </>
        </SafeAreaView>
      );
}
function FlatListItemVatTuThuongDung ({item}){

  const buttonDelereAlert = () =>
    Alert.alert(
      "Thông báo",
      "Bạn có muốn xoá vật tư " + `${item.TENVT}`  + " không?",
      [
        {
          text: "Không",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "Có", 
          onPress: () => {
            deleteItemById(item.MAVT);           
          }
        }
      ],
      { cancelable: false }
    );

  return (
      <View style={styles.cardView}>     
            <View style = {styles.wrapper}/> 
                  <View style = {styles.viewContainerFlatlist}>
                      <Text style={styles.tenvattu}>{item.TENVT}</Text>  
                      <Text style={styles.slcongty}>{item.SOLUONG_MIENPHI}</Text>
                      <Text style={styles.slkhachhang}>{item.SOLUONG_TINHTIEN}</Text>
                      <TouchableOpacity 
                        style={styles.deleteBtn}
                        onPress = {() => {
                          buttonDelereAlert();
                        }}
                      >
                        <FontAwesome 
                          name = 'minus-circle'
                          color = '#ff0000'
                          size = {20}
                        />
                      </TouchableOpacity>
                  </View>    
                  <View style = {styles.wrapper}/>                       
      </View>  
  )
}

const styles = StyleSheet.create({
  wrapper: {
    borderBottomColor: '#dcdcdc',
    borderBottomWidth: 1,
},
rowContainer : {
  flex : 1, 
  alignItems: 'flex-start',
},
title : {
  flex : 2, 
  fontSize : 15,
},
item : {
  flex : 7, 
  marginRight : 5
},
iconPhone : {
  flex : 4, 
},
rows : {
  flexDirection : 'row', 
  height : 50, 
  alignItems :'center', 
  justifyContent : 'center',
  marginLeft : 5,
  marginRight : 5,
},
btnButton : {
  width : '30%',
  height : 45,
  borderColor : 'blue',
  borderRadius : 10,
  marginVertical : 10,
  borderWidth : 0,
  backgroundColor : '#4169e1',
},
submitText : {
  fontSize : 20,
  fontWeight : 'bold',
  color : 'white',
  alignSelf : 'center',
  marginVertical : 10
},
viewContainerFlatlist : {
  flex : 1,
  flexDirection : 'row',
  marginBottom : 8,
  justifyContent : 'center',
  alignItems : 'center'
}, 
tenvattu : {
  flex : 5,
  margin : 3,
  marginLeft : 10,
  fontWeight : 'bold', 
},
slcongty : {
  flex : 1,
  margin : 3,
  fontWeight : 'bold',
  fontStyle : 'italic', 
},
slkhachhang : {
  flex : 1,
  margin : 3,
  fontWeight : 'bold',
  fontStyle : 'italic', 
},
deleteBtn : {
  flex : 1,
  margin : 3,
},
titleVT : {
  fontSize : 15,
  fontWeight : 'bold',
  fontStyle : 'italic',
},
anchor : {
  // flex: 1,
  justifyContent: 'flex-start',
  marginBottom: 36
},
textInput : {
  fontSize : 20, 
  fontWeight : 'bold',
  backgroundColor : '#fffaf0', 
  color : '#000000'
},
});