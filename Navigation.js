import *  as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createDrawerNavigator} from '@react-navigation/drawer';

//dùng chung
import {Login} from './src/screens/Login';
import {TimKiemVatTuScreen} from './src/screens/TimKiemVatTuScreen';
import {CustomeDrawerContent} from './src/common/CustomeDrawerContent';
import {GoogleMapDirection} from './src/screens/GoogleMapDirection';

//kỹ thuật
import {PhanCongSuaChuaScreen} from './src/screens/kythuat/PhanCongSuaChuaScreen';
import {PhanCongLapMoiScreen} from './src/screens/kythuat/PhanCongLapMoiScreen';
import {TaoMoiThongTinSuaChua_KyThuat_Screen} from './src/screens/kythuat/TaoMoiThongTinSuaChua_KyThuat_Screen';
import {TheoDoiCongViecScreen} from './src/screens/kythuat/TheoDoiCongViecScreen';

//công nhân
import {HomeScreen} from './src/screens/congnhan/HomeScreen';
import {CongViecDaLamScreen} from './src/screens/congnhan/CongViecDaLamScreen';
import {CapNhatThongTinSuaChuaScreen} from './src/screens/congnhan/CapNhatThongTinSuaChuaScreen';
import {CapNhatThongTinLapMoiScreen} from './src/screens/congnhan/CapNhatThongTinLapMoiScreen';
import {TaoThongTinSuaChua_CongNhan_Screen} from './src/screens/congnhan/TaoThongTinSuaChua_CongNhan_Screen';

const Stack = createStackNavigator();

const navOptionHandler = () => ({
    //headerShown : false,
    headerStyle: {
     backgroundColor: '#2f4f4f',
   },
   headerTintColor: '#fff',
   headerTitleStyle: {
     fontWeight: 'bold',
   },
 })

const StackHome = createStackNavigator();

function HomeStack(){
  return(
    <StackHome.Navigator initialRouteName="Home" headerMode={'none'}>
      <StackHome.Screen name = "Home" component = {HomeScreen} options = {navOptionHandler, {gestureEnabled : false}}/>  
      <StackHome.Screen name = "CongViecDaLam" component = {CongViecDaLamScreen} options = {navOptionHandler, {gestureEnabled : false}}/>      
      <StackHome.Screen name = "CapNhatThongTinSuaChua" component = {CapNhatThongTinSuaChuaScreen} options = {navOptionHandler, {gestureEnabled : false}}/> 
      <StackHome.Screen name = "CapNhatThongTinLapMoi" component = {CapNhatThongTinLapMoiScreen} options = {navOptionHandler, {gestureEnabled : false}}/> 
      <StackHome.Screen name = "TimKiemVatTu" component = {TimKiemVatTuScreen} options = {navOptionHandler, {gestureEnabled : false}}/>  
      <StackHome.Screen name = "TaoThongTinSuaChua_CongNhan" component = {TaoThongTinSuaChua_CongNhan_Screen} options = {navOptionHandler, {gestureEnabled : false}}/>  
      <StackHome.Screen name = "GoogleMapDirection" component = {GoogleMapDirection} options = {navOptionHandler, {gestureEnabled : false}}/>    
    </StackHome.Navigator>
  )
}

const Drawer = createDrawerNavigator();

function DrawerNavigator({navigation}){
  return(
    <Drawer.Navigator initialRouteName="HomeTab"
    drawerContent = {()=> <CustomeDrawerContent navigation={navigation}/>}
    >
      <Drawer.Screen name="HomeTab" component={HomeStack} options = {navOptionHandler}/>
    </Drawer.Navigator>
  )
}

const DrawerPhanCong = createDrawerNavigator();

function DrawerPhanCongNavigator({navigation}){
  return(
    <DrawerPhanCong.Navigator initialRouteName="PhanCongSuaChua"
    drawerContent = {()=> <CustomeDrawerContent navigation={navigation}/>}
    >
      <DrawerPhanCong.Screen name="PhanCongSuaChua" component={PhanCongSuaChuaScreen} options = {navOptionHandler}/>
      <DrawerPhanCong.Screen name="PhanCongLapMoi" component={PhanCongLapMoiScreen} options = {navOptionHandler}/>
      <DrawerPhanCong.Screen name="TaoMoiThongTinSuaChua_KyThuat" component={TaoMoiThongTinSuaChua_KyThuat_Screen} options = {navOptionHandler}/>
      <DrawerPhanCong.Screen name="TheoDoiCongViecScreen" component={TheoDoiCongViecScreen} options = {navOptionHandler}/>
    </DrawerPhanCong.Navigator>
  )
}

export default function Navigation(props)
{
    return(
        <NavigationContainer>
            <Stack.Navigator initialRouteName = 'Login' headerMode={'none'} >
                <Stack.Screen name = 'Login' component = {Login} options = {navOptionHandler} />
                <Stack.Screen name = "HomeApp" component = {DrawerNavigator} options = {navOptionHandler , {gestureEnabled : false}}/>
                <Stack.Screen name = "QuanLyTab" component = {DrawerPhanCongNavigator} options = {navOptionHandler , {gestureEnabled : false}}/>
            </Stack.Navigator>
        </NavigationContainer>
    )
}


